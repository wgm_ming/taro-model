import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './kind.scss'

import movieData from '../../utils/movie.json'

//引入自定义列表组件  所有组件都应当首字母大写并且使用大驼峰式命名法（Camel-Case）
import ListModel from '../component/listmodel/listmodel.tsx'

export default class Kind extends Component {
	constructor (props) {
		super(props)
		this.state = {
			list: [{
				id:1,
				name: ''
			}]
		}
			
	}
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '分类',
		navigationBarBackgroundColor: '#1AE6DC'
  }

  componentWillMount () { 
		 //console.log(this.$router.params.state) //获取路由传递的参数
		 this.getMovieData()
	}

  componentDidMount () {
		
	}

  componentWillUnmount () { 
		
	}

  componentDidShow () { 
		// console.log(movieData)
		Taro.setStorage({
			key: 'key',
			data: 'value'
		})
	}

  componentDidHide () { 
		
	}
	
	getMovieData () {
		let _this = this
		Taro.request({
			url: 'http://t.yushu.im/v2/movie/top250'	, // 豆瓣电影top250   http://zt.itbing.net/api/banners
			data: {},
			method: 'GET',
			header: {
				'content-type': 'application/json' // 默认值
			},
			success(res) {
				// console.log(res)
				_this.setState({
					list: res.data.subjects
				})
			}
		})
	}
	
	//跳转详情页
	detailShow (id) {
		// console.log(id)
		Taro.navigateTo({
		  url: '/pages/detail/detail?id=' + id
		})
	}
	
  render () {
		
    return (
      <View className='kind'>	
			<ListModel moviedata={this.state.list} onDetailShow={this.detailShow} sendmsg='父组件传来的值'></ListModel>
      </View>
    )
  }
}

