import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './listmodel.scss'


export default class Index extends Component {
	constructor (props) {
		super(props)
		this.state = {
			data: 'hello taro',
			
		}
	}

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
//   config: Config = {
//     navigationBarTitleText: '首页',
// 		navigationBarBackgroundColor: '#FF03A0'
//   }

  componentWillMount () { 	//页面被载入 在微信小程序中这一生命周期方法对应 onLoad
		// console.log(this.props.moviedata)
	}

  componentDidMount () { 	//页面渲染完成 在微信小程序中这一生命周期方法对应 onReady
		
	}

  componentWillUnmount () {	//页面退出 在微信小程序中这一生命周期方法对应 onUnload
		
	}

  componentDidShow () { //页面展示出来 在微信小程序中这一生命周期方法对应 onShow，在 H5 中同样实现
		
	}

  componentDidHide () { 	//页面被隐藏	 在微信小程序中这一生命周期方法对应 onHide，在 H5 中同样实现
		
	}
	
   
   toDetail (id,e) {
	   //子组件向父组件传递事件 自定义事件名必须以on开头 onDetailShow
	   this.props.onDetailShow(id)
   }
   
   
	
  render () {
	
	let listItem = this.props.moviedata.map((item) => {
		return <View className='list-item' onClick={this.toDetail.bind(this,item.id)} taroKey={item.id}>
			<Image className='item-img' src={item.images.large}></Image>
			<Text>{item.title}</Text>
		</View>
	})
		
    return (
      <View className='list-model'>
			自定义列表组件-->{this.props.sendmsg} 
			{listItem}
      </View>
    )
  }
}

