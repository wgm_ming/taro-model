import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './detail.scss'



class Kind extends Component {
	constructor (props) {
		super(props)
		this.state = {
			text: '',
			detailId: '',
			movieObj: {}
		}
			
	}
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '电影详情',
		navigationBarBackgroundColor: '#FF9500'
  }

  componentWillMount () { 
		console.log(this.$router.params.id)		//获取路由传递的参数
		this.setState({
			detailId: this.$router.params.id
		},() => {		//this.setState 是异步的  拿到参数后 要在其回调中执行后续操作
			//获取电影详情
			this.getDetail()
		})
		
	}

  componentDidMount () {
		
	}

  componentWillUnmount () { 
		
	}

  componentDidShow () { 

	}

  componentDidHide () { 
		
	}
	
	//获取电影详情
	getDetail () {
		let _this = this
		Taro.request({
			url:'http://t.yushu.im/v2/movie/subject/' + _this.state.detailId,
			data:{},
			header: { 
				'content-type': 'application/json' // 默认值 
			},
			success(res) {
				console.log(res)
				_this.setState({
					movieObj: res.data
				}) 
			}
		})
	}
	
	
  render () {	
		
    return (
      <View className='detail'>	
		<View className='img-wrap'>
			<Image className='main-img' src={this.state.movieObj.images.large}/>
		</View>
		<View className='detail-title'>{this.state.movieObj.title}</View>
      </View>
    )
  }
}

