import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Swiper, SwiperItem, Button } from '@tarojs/components'
import './index.scss'
import userImg from '../../images/20181225144736.png'

export default class Index extends Component {
	constructor (props) {
		super(props)
		this.state = {
			data: 'hello taro',
			status: true,
			iptvalue: '0',
			userPhoto: '',
			bannerData: [],
			latitude: '',		//维度
			longitude: ''		//经度
		}
	}

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '首页',
		navigationBarBackgroundColor: '#FF03A0'
  }

  componentWillMount () { 	//页面被载入 在微信小程序中这一生命周期方法对应 onLoad
		//获取轮播信息
		this.getBanner()
		//获取位置信息
		this.getUserLocation()
	}

  componentDidMount () { 	//页面渲染完成 在微信小程序中这一生命周期方法对应 onReady
		
	}

  componentWillUnmount () {	//页面退出 在微信小程序中这一生命周期方法对应 onUnload
		
	}

  componentDidShow () { //页面展示出来 在微信小程序中这一生命周期方法对应 onShow，在 H5 中同样实现
		// console.log('asd')
	}

  componentDidHide () { 	//页面被隐藏	 在微信小程序中这一生命周期方法对应 onHide，在 H5 中同样实现
		
	}
	
	//获取轮播数据
	getBanner () {
		let _this = this
		Taro.request({
			url: 'http://zt.itbing.net/api/banners',
			data: {},
			header: {
				'content-type': 'application/json' // 默认值
			},
			success(res) {
				console.log(res)
				_this.setState({
					bannerData: res.data.data
				})
			}
		})
	}
	
	inputActive = (e) => {
		// console.log(e.detail.value)
		this.setState({
			iptvalue: e.detail.value
		})
	}
	
	//跳转到分类页面
	toKind = (e) => {
		// console.log(11)
		Taro.navigateTo({
			url: '/pages/kind/kind?state=zz'
		})
		//在taro 中this.setState()是异步的，所以不能执行该方法后并不能立刻拿到改变的值，
		//但是this.setState()可以传入第二个参数callback，在该函数中可以拿到改变后的值
		return
		this.setState({
			data: 'hello taro !!!'
		},() => {
			console.log(this.state.data)	//在回调函数中可以获取改变后的值
		})
		console.log(this.state.data)	//不能拿到改变后的值
	}
	
	//获取位置信息
	getUserLocation() {
		let _this = this
		Taro.getLocation({
			type: 'wgs84',
			success(res) {
				console.log(res)
				_this.setState({
					latitude: res.latitude
					longitude: res.longitude
				})
			}
		})
	}
	
	//微信授权 获取用户信息
	onGotUserInfo = (e) => {
		console.log(e.detail.userInfo.avatarUrl)
		this.setState({
			userPhoto: e.detail.userInfo.avatarUrl
		})
	}
	
	
  render () {
		let cal = true
		
		let bannerItem = this.state.bannerData.map((items) => {
			return <SwiperItem taroKey={items.id}>
						<View className='demo-text-1'>
						<Image className='itemimg' src={items.image_text}></Image>
						</View>
					</SwiperItem>
		})
		
		
		
    return (
      <View className='index'>
				<Swiper
					className='swiper-com'
					indicatorColor='#999'
					indicatorActiveColor='#333'
					vertical
					circular
					indicatorDots
					autoplay>
					{bannerItem}
				</Swiper>
				<Image className="img" src={this.state.userPhoto}></Image>
				<Text className="textword" >Hello world!</Text>
				{ cal && <View>{this.state.data}</View>}
				{ !cal || <View>条件为假</View>}
				{this.state.status? <View>已登录</View> : <View>未登录</View>}
				<View className="iptarea">
					<Input className="ipt" onInput={this.inputActive} placeholder="请输入" value={this.state.iptvalue}></Input>
					<Text>{this.state.iptvalue}</Text>
				</View>
				<Map className='map'
					longitude={longitude}
					latitude={latitude}
					show-location
				></Map>
				<Button className="btn" onClick={this.toKind}>分类</Button>
				<Button className="btn" open-type='getUserInfo' onGetUserInfo={this.onGotUserInfo}>授权登录</Button>
      </View>
    )
  }
}

